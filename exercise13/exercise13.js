const events = require('events')
const keikka = new events.EventEmitter()

const artisti = "Martti Sero"
let Olut

keikka.on('alkaa', (artisti) => {
  console.log(`JES! - ${artisti} alkaa laulamaan!`)
  setTimeout( () => {
    keikka.emit('loppuu')
  }, 5000) 
  Olut = setInterval( () => {
    keikka.emit('juodaan olutta!!!')
  }, 1000) 
})

keikka.on('juodaan olutta!!!', function () {
  console.log('KIPPIS - juodaan olutta!!!!')
})

keikka.on('loppuu', function () {
  console.log('OLIPA RATTOISAA - eiku taksilla kotio...')
  clearInterval(Olut)
})

keikka.emit('alkaa', artisti)
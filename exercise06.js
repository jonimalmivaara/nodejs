const path = require("path")

if (process.argv.length <= 4) {
    console.log("Usage: " + path.basename(__filename) + " RANDOMIZED_NUMBERS_COUNT MIN_VALUE MAX_VALUE")
    process.exit(-1)
}

if (
    isNaN(process.argv[2]) || isNaN(process.argv[3]) || isNaN(process.argv[4]) ||
    process.argv[2] <= 0 || process.argv[3] >= process.argv[4]
) {
    console.log("Usage: " + path.basename(__filename) + " RANDOMIZED_NUMBERS_COUNT MIN_VALUE MAX_VALUE")
    process.exit(-1)
}

let count = Number(process.argv[2])
let min = Number(process.argv[3])
let max = Number(process.argv[4])

const createArray = () => {
    let result= []

    for (let i = 0; i < count; i++ ) {
        result.push(getRandomInt(min, max))
    }
    return result
}

const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

const array = createArray()
console.log('Randomized numbers: ' + array)
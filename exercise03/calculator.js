const sum = (num1, num2) => {
    return num1 + num2
}

const subs =  (num1, num2) => {
    return num1 - num2
}

const div = (num1, num2) => {
    return num1 / num2
}

const mult = (num1, num2) => {
    return num1 * num2
}

module.exports = {
    sum, subs, div, mult
}
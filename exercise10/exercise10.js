const fs = require('fs')

const sum = (array) => {
    return array.reduce((total, number) => {
        return total + Number(number)
    }, 0)
}

fs.readFile('numbers.txt', (error, data) => {
    if (error) console.error(error)
    else {
      let array = data.toString().split(',')
      console.log(sum(array))
    }
  })

  console.log("Sum of array numbers is...")
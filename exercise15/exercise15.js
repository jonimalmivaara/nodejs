const http = require('http')
const fs = require('fs')
const path = require('path')

const hostname = '127.0.0.1'
const port = 3000

let visitCount = 1;

const server = http.createServer((request, response) => {
    request = visitCount++;
    request = fs.writeFile('visitcount.txt', `Recuest counter value ${visitCount}` , err => {
        if (err) console.error(error)
        else console.log("visit counted and stored")
    })
    response.statusCode = 200
    response.setHeader('Content-Type', 'text/plain')
    response.end(`Request counter value ${visitCount}`)
})

server.listen(port, hostname, () => {
    console.log('Server up and runing at http://${hostname}:${port}/')
    console.log(visitCount)
})
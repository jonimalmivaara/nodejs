const sum = (num1, num2) => {
    return num1 + num2
}

const subs =  (num1, num2) => {
    return num1 - num2
}

const div = (num1, num2) => {
    return num1 / num2
}

const mult = (num1, num2) => {
    return num1 * num2
}

let num1 = 12
let num2 = 11

console.log(`${num1} + ${num2} = ${sum(num1,num2)}`)
console.log(`${num1} - ${num2} = ${subs(num1,num2)}`)
console.log(`${num1} + ${num2} = ${div(num1,num2)}`)
console.log(`${num1} * ${num2} = ${mult(num1,num2)}`)
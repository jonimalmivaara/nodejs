const fs = require('fs')
const internal = require('stream')

const avg = (avgArray) => {
  return avgArray.reduce( (total, number) => { 
    return total + Number(number) / avgArray.length
    }, 0)
}

fs.readFile('ninja.txt', (error, data) => {
  if (error) console.error(error)
  else {
    let array = data.toString()
    let avgArray = data.toString().match(/\d/g)
    console.log(array)
    console.log("Your JavaScript Ninja value is :",avg(avgArray))
  }
})


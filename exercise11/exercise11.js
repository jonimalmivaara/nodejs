const fs = require('fs')
const path = require('path')

if (process.argv.length <= 2) {
  console.log("write text next to file name")
  process.exit(-1)
}

let text = process.argv[2]

fs.writeFile('file.txt', text, error => {
  if (error) console.error(error)
  else console.log('file saved')
})

console.log('saving text to file...')